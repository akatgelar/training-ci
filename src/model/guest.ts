import { db } from '../lib';
import { Instance, INTEGER, STRING } from 'sequelize';

/**
 * Guest data structure
 * guest information recorded on database
 *
 * @export
 * @interface GuestRow
 */
export interface GuestRow{
    id?: number;
    name: string;
    address: string;
    phone: string;
}

/**
 * define guest data structure
 *
 * @export
 * @returns
 */
export function defineGuestModel(){
    return db.define<Instance<GuestRow>, GuestRow>(
        'guest',
        {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            name: {
                type: STRING,
                allowNull: false,
                unique: true
            },
            address: STRING,
            phone: STRING
        },
        { timestamps: false }
    );
}