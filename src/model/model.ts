import { defineGuestModel } from './guest';
import { logger } from '../lib';

/**
 * Define all model definition
 *
 * @export
 */
export function defineModel(){
    logger.debug('start define model');
    defineGuestModel();
    logger.debug('finish define model');
}