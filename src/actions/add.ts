import { Request, Response} from 'express';
import { ERROR_MESSAGE, logger } from '../lib';

/**
 * Handling add function from server
 * POST /add {number1, number2}
 *
 * @export
 * @param {Request} req request object
 * @param {Response} res response object
 * @returns
 */
export function add (req:Request, res:Response){
    const {number1, number2} = req.body;
    logger.trace('receive add request');
    if(!number1 || !number2){
        logger.error('parameter missing on add request');
        return res.status(400).send({
            ok: false,
            result: null,
            error: ERROR_MESSAGE.BAD_AGRUMENT
        });
    }

    logger.debug('make addition ', { number1, number2 });
    const result = number1 + number2;
    logger.trace('addition result ', {result});
    return res.json({
        ok: true,
        result,
        error: null
    });
}