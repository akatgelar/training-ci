import * as bunyan from 'bunyan';
import { config } from '../config';

export const logger = bunyan.createLogger({
    name: 'training',
    level: config.LOG_LEVEL
});