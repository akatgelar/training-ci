import * as SequalizeI from 'sequelize';
import { Sequelize } from 'sequelize';
import { config } from '../config';
import { defineModel } from '../model/model';
import { logger } from './logger';

export const db:Sequelize = new SequalizeI({
    dialect: 'mysql',
    logging: false,
    database: config.DB.DATABASE,
    username: config.DB.USERNAME,
    password: config.DB.PASSWORD,
    host: config.DB.HOST,
    port: config.DB.PORT
});


/**
 * synchronizing database
 *
 * @export
 */
export async function syncDB(){
    logger.debug('sync database');
    defineModel();
    try {
        await db.sync();
        logger.debug('finish synchronizing db');
    } catch (error) {
        logger.error(error);
    }
}