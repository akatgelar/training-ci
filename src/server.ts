import { add } from './actions/add';
import { syncDB, logger, db } from './lib';
import { config } from './config';

const Express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const app = Express();

app.use(cors());

app.post('/add', bodyParser.json(), add);

const server = http.createServer(app);

/**
 * Run Server
 *
 * @export
 */
export async function run() {
  await syncDB();
  server.listen(config.APP_PORT, () => {
    logger.info(`server jalan di port ${config.APP_PORT}`);
  });
}

/**
 * Stop Server
 *
 * @export
 */
export async function stop() {
  logger.info(`stopping app`);
  await db.close();
  logger.info(`database stopped`);
  server.close(() => {
    logger.info(`server stopped`);
  });
}
