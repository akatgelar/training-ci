import * as fetch from 'node-fetch';
import { config } from '../config';

export interface RequestResult<T> {
    ok: boolean;
    result: T;
    error: string | Error;
}

export async function apiRequest<T>(
    command: string,
    args: any,
    method: string = 'POST'
    ) : Promise<RequestResult<T>> {
    const result = await fetch(`http://localhost:${config.APP_PORT}/${command}`, {
        body: JSON.stringify(args),
        mode: 'cors',
        method,
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    });

    let output: RequestResult<T>;
    output = (await result.json()) as RequestResult<T>;
    return output;
}