import * as rc from 'rc';
import { LogLevel } from 'bunyan';

export interface Config{
    APP_PORT: number;
    LOG_LEVEL: LogLevel;
    DB: {
        USERNAME: string;
        PASSWORD: string;
        DATABASE: string;
        HOST: string;
        PORT: number;
    };
}

export const defaultConfig: Config = {
    APP_PORT: 1111,
    LOG_LEVEL: 'debug',
    DB: {
        USERNAME: 'root',
        PASSWORD: 'password',
        DATABASE: 'training-sirus-test',
        HOST: 'localhost',
        PORT: 3306
    }
};

export const config: Config = rc<Config>('training', defaultConfig);