import {run as start, stop} from '../../dist/server';
import * as waitOn from 'wait-on';
import { config } from './config';

describe('Operation Server', function(){

    before(function(done){
        this.timeout(8000);
        start();
        waitOn(
            {
                resources: [`tcp:localhost:${config.APP_PORT}`],
                timeout: 6000
            },
            function(err){
                done(err);
            }
        );
    });

    after(function(){
        stop();
    });

    require('./add.spec');
});