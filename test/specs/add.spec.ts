import { expect } from 'chai';
import { ERROR_MESSAGE, apiRequest } from './lib';

describe('Addition', function(){
    it('should add first number with second number', async function(){
        const number1 = (Math.random() * 500) + 500;
        const number2 = (Math.random() * 500) + 500;
        const output = await apiRequest('add', {
            number1,
            number2
        });

        expect(output.ok).to.be.true;
        expect(output.result).to.be.deep.equal(number1 + number2);
        expect(output.error).to.be.null;

    });

    it('should return error input invalid', async function(){
        const input1 = (Math.random() * 500) + 500;
        const output = await apiRequest('add', {
                number1: input1
        });

        expect(output.ok).to.be.false;
        expect(output.result).to.be.null;
        expect(output.error).to.be.equal(ERROR_MESSAGE.BAD_AGRUMENT);

    });
});