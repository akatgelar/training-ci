FROM node:8-alpine

RUN mkdir /app
WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install

COPY . /app
RUN npm run build && npm run test:build

ENTRYPOINT [ "npm", "run"]
CMD [ "start"]