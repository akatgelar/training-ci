# Training CI

[![pipeline status](https://gitlab.com/akatgelar/training-ci/badges/master/pipeline.svg)](https://gitlab.com/akatgelar/training-ci/commits/master)
[![coverage report](https://gitlab.com/akatgelar/training-ci/badges/master/coverage.svg)](https://gitlab.com/akatgelar/training-ci/commits/master)


1. [instalasi](#instalasi)
2. [pengujian](#pengujian-aplikasi)
3. [pengembangan](#pengembangan)
4. [dokumentasi](#dokumentasi)

## Instalasi
untuk melakukan instalasi jalankan command berikut

```bash
npm install
npm run build
```

## Pengujian Aplikasi
untuk melakukan pengujian jalankan command berikut

```bash
npm run test:build
npm test
```

untuk melakukan pengembangan test

```bash
npm run test:dev
```

## Pengembangan
untuk melakukan pengembangan jalankan command berikut

```bash
npm run dev
```

## Dokumentasi
untuk melakukan dokumentasi jalankan command berikut

```bash
npm run docs
```